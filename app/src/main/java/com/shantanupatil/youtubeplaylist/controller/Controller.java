package com.shantanupatil.youtubeplaylist.controller;

import com.shantanupatil.youtubeplaylist.interfaces.IPassData;

public class Controller {
    IPassData passData;

    public Controller(IPassData passData) {
        this.passData = passData;
    }

    public void onItemClick(String videoID) {
        passData.passData(videoID);
    }
}
