package com.shantanupatil.youtubeplaylist.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.lang.reflect.Type;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import com.shantanupatil.youtubeplaylist.R;
import com.shantanupatil.youtubeplaylist.controller.Controller;
import com.shantanupatil.youtubeplaylist.database.SQLHelper;
import com.shantanupatil.youtubeplaylist.models.YoutubeModel;

public class YoutubeAdapter extends RecyclerView.Adapter<YoutubeAdapter.ViewHolder>{

    private static final String TAG = "YoutubeAdapterDebugging";
    private List<YoutubeModel> youtubeModelList;
    private Context context;
    private final int HEAD = 0;
    private final int LIST = 1;
    Controller controller;

    public YoutubeAdapter(List<YoutubeModel> youtubeModelList, Context context, Controller controller) {
        this.youtubeModelList = youtubeModelList;
        this.context = context;
        this.controller = controller;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        ViewHolder viewHolder;

        if (viewType == LIST) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.youtubelist_item, parent, false);
            viewHolder = new ViewHolder(view, viewType);
            return viewHolder;
        } else if (viewType == HEAD) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.youtubelist_item_head, parent, false);
            viewHolder = new ViewHolder(view, viewType);
            return viewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final YoutubeModel youtubeModel = youtubeModelList.get(position);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "circularstd.ttf");
        final SQLHelper sqlHelper = new SQLHelper(context);
        final int[] flag = {1};

        if (holder.view_type == LIST) {
            holder.listTitle.setText(youtubeModel.getTitle());
            holder.publishedAt.setText(getPublishAtDate(youtubeModel.getPublishedAt()));
            holder.description.setText(youtubeModel.getDescription());
            holder.listTitle.setTypeface(typeface);
            holder.listTitle.setTextColor(Color.BLACK);
            Glide.with(context)
                    .load(youtubeModel.getThumbnail())
                    .apply(new RequestOptions()
                    .placeholder(R.drawable.circle_placeholder))
                    .into(holder.listThumb);

            holder.favourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Cursor cursor = sqlHelper.getAllData();
                    while (cursor.moveToNext()) {
                        String temp_id = cursor.getString(2);
                        if (temp_id.equals(youtubeModel.getVideoID())) {
                            flag[0] = 0;
                        }
                    }
                    if (flag[0] != 0) {
                        boolean res = sqlHelper.insertData(youtubeModel.getTitle(), youtubeModel.getVideoID(), youtubeModel.getThumbnail(), youtubeModel.getDescription(), youtubeModel.getPublishedAt());
                        if (res) {
                            //holder.favourite.setImageResource(R.drawable.yesfavourite);
                            Toast.makeText(context, "Added to favourite", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Already Favourite", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else if (holder.view_type == HEAD) {
            holder.headTitle.setText(youtubeModel.getTitle());
            holder.headTitle.setTypeface(typeface);
            Glide.with(context).load(youtubeModel.getThumbnail()).into(holder.headThumb);
        }
    }

    private String getPublishAtDate(String publishedAt) {
        String[] arrayPubliished = publishedAt.split("-");
        return arrayPubliished[2].substring(0, 2) + " " + getMonth(arrayPubliished[1]) + " " + arrayPubliished[0];
    }

    private String getMonth(String month) {
        if (month.equals("01")) {
            return "January";
        }
        if (month.equals("02")) {
            return "February";
        }
        if (month.equals("03")) {
            return "March";
        }
        if (month.equals("04")) {
            return "April";
        }
        if (month.equals("05")) {
            return "May";
        }
        if (month.equals("06")) {
            return "June";
        }
        if (month.equals("07")) {
            return "July";
        }
        if (month.equals("08")) {
            return "August";
        }
        if (month.equals("09")) {
            return "September";
        }

        if (month.equals("10")) {
            return "October";
        }

        if (month.equals("11")) {
            return "November";
        }

        if (month.equals("12")) {
            return "December";
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == HEAD)
            return HEAD;
        return LIST;
    }

    @Override
    public int getItemCount() {
        return youtubeModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        int view_type;

        TextView listTitle;
        TextView publishedAt;
        TextView description;
        CircleImageView listThumb;
        LinearLayout listLinearLayout;
        ImageView favourite;

        TextView headTitle;
        //TextView headDescription;
        ImageView headThumb;
        RelativeLayout headLayout;

        public ViewHolder(View itemView, int viewType) {
           super(itemView);
            if (viewType == LIST) {
                listTitle = (TextView) itemView.findViewById(R.id.youtube_list_title);
                favourite = (ImageView) itemView.findViewById(R.id.favourite);
                description = (TextView) itemView.findViewById(R.id.youtube_list_description);
                publishedAt = (TextView) itemView.findViewById(R.id.youtube_list_published_at);
                listThumb = (CircleImageView) itemView.findViewById(R.id.youtube_list_thumbnail);
                listLinearLayout = (LinearLayout) itemView.findViewById(R.id.youtube_list_linear_layout);
                listLinearLayout.setOnClickListener(this);
                view_type = 1;
            } else if (viewType == HEAD) {
                headTitle = (TextView) itemView.findViewById(R.id.youtube_head_title);
                headThumb = (ImageView) itemView.findViewById(R.id.youtube_head_thumb);
                headLayout = (RelativeLayout) itemView.findViewById(R.id.youtube_head_relative_layout);
                headLayout.setOnClickListener(this);
            }

        }

        @Override
        public void onClick(View v) {
            YoutubeModel youtubeModel = youtubeModelList.get(getAdapterPosition());
            controller.onItemClick(youtubeModel.getVideoID());
        }
    }
}
