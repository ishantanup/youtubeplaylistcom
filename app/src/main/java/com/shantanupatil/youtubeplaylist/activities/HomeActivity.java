package com.shantanupatil.youtubeplaylist.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.codemybrainsout.ratingdialog.RatingDialog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.shantanupatil.youtubeplaylist.Config;
import com.shantanupatil.youtubeplaylist.R;
import com.shantanupatil.youtubeplaylist.adapter.FavouriteAdapter;
import com.shantanupatil.youtubeplaylist.adapter.YoutubeAdapter;
import com.shantanupatil.youtubeplaylist.controller.Controller;
import com.shantanupatil.youtubeplaylist.database.SQLHelper;
import com.shantanupatil.youtubeplaylist.interfaces.IPassData;
import com.shantanupatil.youtubeplaylist.models.YoutubeModel;

public class HomeActivity extends AppCompatActivity implements IPassData{

    private static final String TAG = "HomeActivityDebugging";
    private Config config;
    private int count = 0;

    private DrawerLayout homeDrawer;
    private ActionBarDrawerToggle homeToggle;
    private Toolbar toolbar;

    private RecyclerView homeRecycler;
    private YoutubeAdapter youtubeAdapter;
    private FavouriteAdapter favouriteAdapter;
    private ProgressBar progressBarHome;
    Controller controller;

    List<YoutubeModel> youtubeModelList;

    AdView adView;
    private InterstitialAd mInterstitialAd;
    RatingDialog ratingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ratingDialog = new RatingDialog.Builder(this)
                .session(5)
                .title("How was your experience with us?")
                .titleTextColor(R.color.black)
                .positiveButtonText("Not Now")
                .negativeButtonText("Never")
                .build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[] {"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}, 0);
        }

        adView = (AdView) findViewById(R.id.adview);
        config = new Config();

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(config.INTERSTITIAL);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mInterstitialAd.loadAd(adRequest);

        showAd();

        //setting the banner ad
        AdRequest bannerAd = new AdRequest.Builder().build();
        adView.loadAd(bannerAd);

        toolbar = (Toolbar) findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);

        controller = new Controller(this);

        youtubeModelList = new ArrayList<>();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.app_name);

        progressBarHome = (ProgressBar) findViewById(R.id.home_progress_bar);

        /*********************************Navigation Menu Code************************************************/
        homeDrawer = (DrawerLayout) findViewById(R.id.home_drawer_layout);
        homeToggle = new ActionBarDrawerToggle(this, homeDrawer, R.string.open, R.string.close);
        homeDrawer.addDrawerListener(homeToggle);
        homeToggle.syncState();

        /*********************************RecyclerView Setup************************************************/
        homeRecycler = (RecyclerView) findViewById(R.id.home_recycler);
        homeRecycler.setHasFixedSize(true);
        homeRecycler.setLayoutManager(new LinearLayoutManager(this));


        NavigationView navigationView = (NavigationView) findViewById(R.id.home_navigation);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.nav_play_list) {
                    homeRecycler.setVisibility(View.GONE);
                    progressBarHome.setVisibility(View.VISIBLE);
                    loadYoutubeData(config.URL_LINK);
                    homeDrawer.closeDrawers();
                } else if (item.getItemId() == R.id.nav_favourite) {
                    homeRecycler.setVisibility(View.GONE);
                    progressBarHome.setVisibility(View.VISIBLE);
                    getFavouriteData();
                    homeDrawer.closeDrawers();
                }
                return false;
            }
        });

        loadYoutubeData(config.URL_LINK);

    }

    private void getFavouriteData() {
        final List<YoutubeModel> youtubeModels = new ArrayList<>();

        SQLHelper helper = new SQLHelper(this);
        Cursor cursor = helper.getAllData();
        while (cursor.moveToNext()) {
            String title = cursor.getString(1);
            String videoId = cursor.getString(2);
            String image = cursor.getString(3);
            String des = cursor.getString(4);
            String pub = cursor.getString(5);

            YoutubeModel youtubeModel = new YoutubeModel(title, pub, image, des, videoId);
            youtubeModels.add(youtubeModel);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                homeRecycler.setVisibility(View.VISIBLE);
                progressBarHome.setVisibility(View.GONE);
                favouriteAdapter = new FavouriteAdapter(youtubeModels, getApplicationContext(), controller);
                homeRecycler.setAdapter(favouriteAdapter);
            }
        }, 1000);

    }

    private void showAd() {
        mInterstitialAd.setAdListener(new com.google.android.gms.ads.AdListener() {
            @Override
            public void onAdLoaded() {
                mInterstitialAd.show();
                super.onAdLoaded();
            }

            @Override
            public void onAdClosed() {
                ratingDialog.show();
                super.onAdClosed();
            }
        });
    }

    private void loadYoutubeData(String url) {
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray items = response.getJSONArray("items");

                    for (int i = 0; i < items.length(); i++) {
                        JSONObject eachObject = items.getJSONObject(i);

                        //get the snippet object
                        JSONObject snippet = eachObject.getJSONObject("snippet");

                        String title = snippet.getString("title");
                        String publishedAt = snippet.getString("publishedAt");
                        String description = snippet.getString("description");

                        //get thumbnail object
                        JSONObject thumbnailObject = snippet.getJSONObject("thumbnails");
                        JSONObject thumbnailHigh = thumbnailObject.getJSONObject("high");
                        String thumnailURL = thumbnailHigh.getString("url");

                        JSONObject resourceObject = snippet.getJSONObject("resourceId");
                        String videoID = resourceObject.getString("videoId");

                        YoutubeModel youtubeModel = new YoutubeModel(title, publishedAt, thumnailURL, description, videoID);
                        youtubeModelList.add(youtubeModel);
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            homeRecycler.setVisibility(View.VISIBLE);
                            progressBarHome.setVisibility(View.GONE);
                            youtubeAdapter = new YoutubeAdapter(youtubeModelList, getApplicationContext(), controller);
                            homeRecycler.setAdapter(youtubeAdapter);
                        }
                    }, 1000);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        Volley.newRequestQueue(getApplicationContext()).add(jsonArrayRequest);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (homeToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void passData(String id) {
        Intent intent = new Intent(getApplicationContext(), PlayerActivity.class);
        intent.putExtra("videoId", id);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.logo)
                .setTitle("Are you sure?")
                .setMessage("Select No to use the application or go with option Yes if you really want to close the application")
                .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).setNegativeButton("no", null)
                .show();
    }

}
