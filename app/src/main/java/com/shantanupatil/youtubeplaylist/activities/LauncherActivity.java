package com.shantanupatil.youtubeplaylist.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.shantanupatil.youtubeplaylist.R;
import com.shantanupatil.youtubeplaylist.activities.HomeActivity;

public class LauncherActivity extends AppCompatActivity {

    //After 2000 ms the HomeActivity will launch
    private final int TIME_OUT = 3000;

    //Progressbar
    ProgressBar launcherProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        //initialize progressbar
        launcherProgress = (ProgressBar) findViewById(R.id.launcher_progress_bar);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
            }
        }, TIME_OUT);
    }
}
